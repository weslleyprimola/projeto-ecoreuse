package br.ecoreuse.ecoreuseapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcoreuseappApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcoreuseappApplication.class, args);
	}

}
